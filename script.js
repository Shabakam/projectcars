import * as THREE from './vendor/three.js-master/build/three.module.js'; 
import { OrbitControls } from './vendor/three.js-master/examples/jsm/controls/OrbitControls.js';
import Stats from './vendor/three.js-master/examples/jsm/libs/stats.module.js';
import { FBXLoader } from './vendor/three.js-master/examples/jsm/loaders/FBXLoader';

var hitbox;
var wall15;
var wall16;
var victory;

const Scene = {
    vars:{
        container: null,
        scene: null,
        stats: null,
        renderer: null,
        camera: null,
        controls: null,
        keyboard: new THREEx.KeyboardState(),
        clock: new THREE.Clock(),
        collidableMeshList: [],
        victoryMeshList: []
    },
    init: () => {
        let vars = Scene.vars;

        // Préparer le container de la scene
        vars.container = document.createElement('div');
        vars.container.classList.add("fullscreen");
        document.body.appendChild(vars.container);

        // Création de la scène
        vars.scene = new THREE.Scene();
        vars.scene.background = new THREE.Color(0xa0a0a0); 
        
        // Création du moteur de rendu
        vars.renderer = new THREE.WebGLRenderer({ antialias: true });
        vars.renderer.setPixelRatio(window.devicePixelRatio);
        vars.renderer.setSize(window.innerWidth, window.innerHeight);

        // Casting des ombres dans le renderer
        vars.renderer.shadowMap.enabled = true;
        vars.renderer.shadowMapSoft = true; 
        vars.container.appendChild(vars.renderer.domElement); 

        // Création d'une caméra perspective
        vars.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000); 
        vars.camera.position.set(-1.5, 210, 572);

        // Création d'une lumière hémisphérique (une lumière équivalente au soleil)
        let lightIntensityHemisphere = 0.8;
        let light = new THREE.HemisphereLight(0xFFFFFF, 0x444444, lightIntensityHemisphere);

        light.position.set(0, 700, 0);
        vars.scene.add(light); 

        // Création d'un sol à la scène
        let mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(2000, 2000), new
        THREE.MeshLambertMaterial({map: new THREE.TextureLoader().load('./fbx/textureSol.jpg')}));
        mesh.rotation.x = -Math.PI / 2;
        vars.scene.add(mesh);
        

        // Chargement de la voiture
        Scene.loadFBX("car.fbx", 10, [0, 10, 0], [0, 0, 0], 0xFFFFFF, "car", () => {
            // Positionnement de la voiture
            
            var carFinished = Scene.addFBX(Scene.vars.car, -100, 10, 0, 0, -Math.PI /2, 0, 0xFFD700);
            Scene.vars.carFinal = carFinished;

            // Création d'une lumière directionnelle
            let lightIntensity = 0.5;
            let directional = new THREE.DirectionalLight(0xffffff, lightIntensity);
            
            directional.position.set(0, 200, 300); 
            
            directional.target = carFinished;
            
            // Ombres de la lumière directionnelle
            // let d = 1000;

            // directional.castShadow = true;
            // directional.shadow.camera.left = -d;
            // directional.shadow.camera.right = d;
            // directional.shadow.camera.top = d;
            // directional.shadow.camera.bottom = -d;
            // directional.shadow.camera.far = 2000;
            // directional.shadow.mapSize.width = 4096;
            // directional.shadow.mapSize.height = 4096; 

            // let helper = new THREE.CameraHelper(directional.shadow.camera);
            // vars.scene.add(helper);

            // vars.scene.add(directional);

            // let planeMaterial = new THREE.ShadowMaterial();
            // planeMaterial.opacity = 0.07;
            // let shadowPlane = new THREE.Mesh(new THREE.PlaneBufferGeometry(2000,
            // 2000), planeMaterial);
            // shadowPlane.rotation.x = -Math.PI / 2;
            // shadowPlane.receiveShadow = true;
            // vars.scene.add(shadowPlane);
        });

        //Hitbox de la voiture
        var cubeGeometry = new THREE.CubeGeometry(23,50,33,2,2,2);
        var wireMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true} );
        hitbox = new THREE.Mesh( cubeGeometry, wireMaterial );
        hitbox.visible = false;
        hitbox.position.set(-100, 10, 0);
        hitbox.rotation.set(0, -Math.PI /2, 0);
        vars.scene.add( hitbox );

        //Ajout des murs et des bordures de l'écran
        var shortWallGeometry = new THREE.CubeGeometry( 100, 100, 20, 1, 1, 1 );
        var mediumWallGeometry = new THREE.CubeGeometry( 200, 100, 20, 1, 1, 1 );
        var longWallGeometry = new THREE.CubeGeometry( 300, 100, 20, 1, 1, 1 );
        var borderGeometry = new THREE.CubeGeometry( 1500, 100, 20, 1, 1, 1 );

        //Texture des murs
        var wallMaterial = new THREE.MeshBasicMaterial( {map: new THREE.TextureLoader().load('./fbx/textureMur.jpg')} );

        var borderHaut = new THREE.Mesh(borderGeometry, wallMaterial);
        borderHaut.position.set(0, 50, 350);
        vars.scene.add(borderHaut);
        vars.collidableMeshList.push(borderHaut);

        var borderBas = new THREE.Mesh(borderGeometry, wallMaterial);
        borderBas.position.set(0, 50, -350);
        vars.scene.add(borderBas);
        vars.collidableMeshList.push(borderBas);

        var borderGauche = new THREE.Mesh(borderGeometry, wallMaterial);
        borderGauche.position.set(-570, 50, 0);
        borderGauche.rotation.y = Math.PI/2;
        vars.scene.add(borderGauche);
        vars.collidableMeshList.push(borderGauche);

        var borderDroit = new THREE.Mesh(borderGeometry, wallMaterial);
        borderDroit.position.set(570, 50, 0);
        borderDroit.rotation.y = Math.PI/2;
        vars.scene.add(borderDroit);
        vars.collidableMeshList.push(borderDroit);

        var wall = new THREE.Mesh(longWallGeometry, wallMaterial);
        wall.position.set(0, 50, -70);
        vars.scene.add(wall);
        vars.collidableMeshList.push(wall);

        var wall2 = new THREE.Mesh(shortWallGeometry, wallMaterial);
        wall2.position.set(-150, 50, 0);
        wall2.rotation.y = Math.PI/2;
        vars.scene.add(wall2);
        vars.collidableMeshList.push(wall2);

        var wall3 = new THREE.Mesh(longWallGeometry, wallMaterial);
        wall3.position.set(0, 50, 70);
        vars.scene.add(wall3);
        vars.collidableMeshList.push(wall3);

        var wall4 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall4.position.set(250, 50, 0);
        wall4.rotation.y = Math.PI/4;
        vars.scene.add(wall4);
        vars.collidableMeshList.push(wall4);

        var wall5 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall5.position.set(250, 50, -110);
        wall5.rotation.y = -Math.PI/4;
        vars.scene.add(wall5);
        vars.collidableMeshList.push(wall5);

        var wall6 = new THREE.Mesh(longWallGeometry, wallMaterial);
        wall6.position.set(50, 50, -145);
        vars.scene.add(wall6);
        vars.collidableMeshList.push(wall6);

        var wall7 = new THREE.Mesh(longWallGeometry, wallMaterial);
        wall7.position.set(-170, 50, -180);
        wall7.rotation.y = Math.PI/2;
        vars.scene.add(wall7);
        vars.collidableMeshList.push(wall7);

        var wall8 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall8.position.set(400, 50, -200);
        wall8.rotation.y = -Math.PI/4;
        vars.scene.add(wall8);
        vars.collidableMeshList.push(wall8);

        var wall9 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall9.position.set(400, 50, -200);
        wall9.rotation.y = Math.PI/4;
        vars.scene.add(wall9);
        vars.collidableMeshList.push(wall9);

        var wall10 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall10.position.set(480, 50, 0);
        vars.scene.add(wall10);
        vars.collidableMeshList.push(wall10);

        var wall11 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall11.position.set(250, 50, 250);
        wall11.rotation.y = Math.PI/2;
        vars.scene.add(wall11);
        vars.collidableMeshList.push(wall11);

        var wall12 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall12.position.set(120, 50, 180);
        wall12.rotation.y = Math.PI/2;
        vars.scene.add(wall12);
        vars.collidableMeshList.push(wall12);

        var wall13 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall13.position.set(10, 50, 250);
        wall13.rotation.y = Math.PI/2;
        vars.scene.add(wall13);
        vars.collidableMeshList.push(wall13);

        var wall14 = new THREE.Mesh(mediumWallGeometry, wallMaterial);
        wall14.position.set(-80, 50, 180);
        wall14.rotation.y = Math.PI/2;
        vars.scene.add(wall14);
        vars.collidableMeshList.push(wall14);

        //Les deux murs pivotants
        wall15 = new THREE.Mesh(longWallGeometry, wallMaterial);
        wall15.position.set(-320, 50, 160);
        wall15.rotation.y = Math.PI/4;
        vars.scene.add(wall15);
        vars.collidableMeshList.push(wall15);

        wall16 = new THREE.Mesh(longWallGeometry, wallMaterial);
        wall16.position.set(-320, 50, 160);
        wall16.rotation.y = -Math.PI/4;
        vars.scene.add(wall16);
        vars.collidableMeshList.push(wall16);

        var wall17 = new THREE.Mesh(longWallGeometry, wallMaterial);
        wall17.position.set(-400, 50, -60);
        vars.scene.add(wall17);
        vars.collidableMeshList.push(wall17);

        //Sphère condition de victoire
        var geometrySphere = new THREE.SphereGeometry( 25, 32, 32 );
        var materialSphere = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
        victory = new THREE.Mesh(geometrySphere, materialSphere);
        victory.position.set(-400, 50, -200);
        vars.scene.add(victory);
        vars.victoryMeshList.push(victory);

        // create an AudioListener and add it to the camera
        var listener = new THREE.AudioListener();
        vars.camera.add( listener );

        // create a global audio source
        var sound = new THREE.Audio( listener );

        // load a sound and set it as the Audio object's buffer
        var audioLoader = new THREE.AudioLoader();
        audioLoader.load( 'sound/f-zero.mp3', function( buffer ) {
            sound.setBuffer( buffer );
            sound.setLoop( true );
            sound.setVolume( 0.25 );
            sound.play();
        });

        //document.querySelector('#loading').remove();


        // Redimensionnement de la fenêtre
        window.addEventListener('resize', Scene.onWindowResize, false); 

        // Ajout de contrôles
        vars.controls = new OrbitControls(vars.camera, vars.renderer.domElement);

        // Limites des contrôles
        vars.controls.maxAzimuthAngle = 0;
        vars.controls.minAzimuthAngle = 0;
        vars.controls.maxPolarAngle = 0;
        vars.controls.minPolarAngle = 0;
        vars.controls.maxDistance = 800;
        vars.controls.minDistance = 800;
        vars.controls.enableKeys = false;

        // Suite ajout contrôles
        vars.controls.target.set(0, 100, 0);
        vars.controls.update();

        // Ajout des stats
        vars.stats = new Stats();
        vars.container.appendChild(vars.stats.dom);
        Scene.animate();
    },
    addFBX: (element,px,py,pz,rx,ry,rz,color) => {
        var unFBX = element.clone();
        Scene.vars.scene.add(unFBX);
        unFBX.position.set(px,py,pz);
        unFBX.rotation.set(rx,ry,rz);

        return unFBX;
    },
    loadFBX: (file, echelle, position, rotation, couleur, nom, callback) => {
        let loader = new FBXLoader();

        if(file === undefined){
            return;
        }
        
        loader.load("./fbx/" + file, function(model){

            model.scale.set(echelle, echelle, echelle);
            model.position.set(position[0], position[1], position[2]);
            model.rotation.set(rotation[0], rotation[1], rotation[2]);

            model.traverse(node => {
                if(node.isMesh){
                    node.castShadow = true;
                    node.receiveShadow = true;
                    node.material.color = new THREE.Color(couleur);
                }
            });
            
            Scene.vars[nom] = model;
            callback();
        });
    },
    //Gestion du redimensionnement de la fenêtre
    onWindowResize: () => {
        let vars = Scene.vars;
        vars.camera.aspect = window.innerWidth / window.innerHeight;
        vars.camera.updateProjectionMatrix();
        vars.renderer.setSize(window.innerWidth, window.innerHeight);
    },
    // Lancement de l'animation
    animate: () => {
        requestAnimationFrame(Scene.animate);

        //Rotation continue des murs à gauche de l'écran
        wall15.rotation.y -= 0.015
        wall16.rotation.y -= 0.015

        Scene.render();
        Scene.update();
    },
    update: () =>{
        //Contrôles de la voiture ZQSD / flèches du clavier
        //Tutoriel trouvé sur http://stemkoski.github.io/Three.js/Mesh-Movement.html
        var delta = Scene.vars.clock.getDelta(); // Secondes
        var moveDistance = 200 * delta; // 200 pixels par seconde
        var rotateAngle = Math.PI / 2 * delta; // pi/2 radians (90 degrés) par seconde
        var keyboard = Scene.vars.keyboard;
        var carFinal = Scene.vars.carFinal;

        if (keyboard.pressed("Z") || keyboard.pressed("up")){
            hitbox.translateZ(-moveDistance);
            carFinal.translateZ(-moveDistance);
        }
        if (keyboard.pressed("S") || keyboard.pressed("down")){
            hitbox.translateZ(moveDistance);
            carFinal.translateZ(moveDistance);
        }
        if (keyboard.pressed("Q") || keyboard.pressed("left")){
            hitbox.rotateOnAxis(new THREE.Vector3(0,1,0), rotateAngle);
            carFinal.rotateOnAxis(new THREE.Vector3(0,1,0), rotateAngle);
        }
	    if (keyboard.pressed("D") || keyboard.pressed("right")){
            hitbox.rotateOnAxis(new THREE.Vector3(0,1,0), -rotateAngle);
            carFinal.rotateOnAxis(new THREE.Vector3(0,1,0), -rotateAngle);
        }

        var originPoint = hitbox.position.clone();
    
        //Détection lors d'une collision avec un mur
        //Tutoriel trouvé sur http://stemkoski.github.io/Three.js/Collision-Detection.html
        for (var vertexIndex = 0; vertexIndex < hitbox.geometry.vertices.length; vertexIndex++)
        {		
            var localVertex = hitbox.geometry.vertices[vertexIndex].clone();
            var globalVertex = localVertex.applyMatrix4( hitbox.matrix );
            var directionVector = globalVertex.sub( hitbox.position );
            
            var ray = new THREE.Raycaster( originPoint, directionVector.clone().normalize() );
            var collisionResults = ray.intersectObjects( Scene.vars.collidableMeshList );
            var collisionResults2 = ray.intersectObjects( Scene.vars.victoryMeshList );
            if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
                hitbox.position.set(-100, 10, 0);
                hitbox.rotation.set(0, -Math.PI / 2, 0);
                carFinal.position.set(-100, 10, 0);
                carFinal.rotation.set(0, -Math.PI / 2, 0);

                var listener = new THREE.AudioListener();
                Scene.vars.camera.add( listener );

                // create a global audio source
                var sound = new THREE.Audio( listener );

                // load a sound and set it as the Audio object's buffer
                var audioLoader = new THREE.AudioLoader();
                audioLoader.load( 'sound/fail.mp3', function( buffer ) {
                    sound.setBuffer( buffer );
                    sound.setVolume( 0.25 );
                    sound.play();
                });
            }

            if (collisionResults2.length > 0 && collisionResults2[0].distance < directionVector.length()) {
                var listener2 = new THREE.AudioListener();
                Scene.vars.camera.add( listener2 );

                // create a global audio source
                var sound2 = new THREE.Audio( listener2 );

                // load a sound and set it as the Audio object's buffer
                var audioLoader2 = new THREE.AudioLoader();
                audioLoader2.load( 'sound/yay.mp3', function( buffer ) {
                    sound2.setBuffer( buffer );
                    sound2.setVolume( 0.25 );
                    sound2.play();
                });

                hitbox.position.set(-100, 10, 0);
                hitbox.rotation.set(0, -Math.PI / 2, 0);
                carFinal.position.set(-100, 10, 0);
                carFinal.rotation.set(0, -Math.PI / 2, 0);
            }
        }

        Scene.vars.controls.update();
	    Scene.vars.stats.update();
    },
    //Render de la scène
    render: () => {
        Scene.vars.renderer.render(Scene.vars.scene, Scene.vars.camera);
        Scene.vars.stats.update();
    }

};
Scene.init(); 